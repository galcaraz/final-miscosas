# by galcaraz 
# User & profile related functions

from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.http import HttpResponseNotFound
from django.db import IntegrityError
from .forms import ProfileForm
from . import models


def getProfile(request): 
    if request.user.is_authenticated:
        return request.user.profile
    else:
        return 0


def userProfile(request, username):
    sessionProfile = getProfile(request)
    user = User.objects.get(username=username)
    requestedProfile = models.Profile.objects.get(user=user)
    if request.method == 'POST':  # new user
        if request.user.is_authenticated:
            form = ProfileForm(request.POST, request.FILES, 
                                instance=sessionProfile)
            if form.is_valid():
                form.save()
    if request.user.username == username:  # if user is in his profile
        form = ProfileForm(instance=sessionProfile)
    else:
        form=''
    data = {'profile': sessionProfile,
            'requestedprofile': requestedProfile,
            'votes': requestedProfile.vote_set.exclude(value=0),  # exclude empty votes
            'comments': requestedProfile.comment_set.all(),
            'feedrecord': requestedProfile.feedRecord.all(),
            'form': form}
    return render(request, "userprofile.html", data)


def allProfiles(request):
    profileList = models.Profile.objects.all()
    answer = []
    for profile in profileList:
        answer.append({'profile': profile,
                        'votes': profile.vote_set.all().exclude(value=0).count(),
                        'comments': profile.comment_set.all().count()})
    data = {"profile": getProfile(request),
            "profilelist": answer}
    return render(request, "profilelist.html", data)


def log(request):
    name = request.POST.get('username')
    pwd = request.POST.get('password')
    log = request.POST.get('log')
    redirectPage = request.POST.get('redirectpage')
    if log == "login":
        try:
            user = User.objects.get(username=name, password=pwd)
        except User.DoesNotExist:
            return HttpResponseNotFound("<h1>User or password don't" + 
                    " match</h1><a href='/'>Try Again</a>")
    elif log == "signup":
        user = User(username=name, password=pwd)
        try:
            user.save()
        except IntegrityError:
            return HttpResponseNotFound("<h1>User already taken!" +
                    "</h1><a href='/'>Try Again</a>")
    else:
        return HttpResponseNotFound("<h1>Page not found </h1>" +
                    "<a href='/'>Home</a>")
    login(request, user)
    return redirect(redirectPage)


def logOut(request):
    logout(request)
    return redirect("home")
