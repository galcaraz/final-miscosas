from django.urls import path
from . import views, users


urlpatterns = [
    path('users/', users.allProfiles, name='users'),
    path('users/log', users.log, name='log'),
    path('users/logout', users.logOut, name='logout'),
    path('users/<str:username>/', users.userProfile, name='userprofile'),
    path('feedlist', views.getFeedList, name='feedlist'),
    path('feed', views.getFeedForm, name='feed'),
    path('feed/<str:id>/', views.getFeed, name='feedid'),
    path('item/<str:key>/', views.getItem, name='item'),
    path('info/', views.infoPage, name='info'), 
    path('', views.home, name='home')
]
