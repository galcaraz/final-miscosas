#by galcaraz
#gets title, thumbnail, id, description and feed from RedditAPI
from django.conf import settings #import private id's and MAX
from django.db.utils import IntegrityError
import praw
from . import models

def RedditSearch(subreddit, refresh):
     feed, created = models.Feed.objects.get_or_create(
                                    server=models.FeedServer('Reddit'),
                                    name=subreddit)
     reddit = praw.Reddit(client_id=settings.REDDIT_CLIENT_ID,
                         client_secret=settings.REDDIT_CLIENT_SECRET,
                         user_agent='mis_cosas_agent')
     if created or refresh:
          for subm in reddit.subreddit(subreddit).hot(limit=settings.MAX_RESULTS):
               try:
                    preview = subm.preview['images'][0]['source']['url'] 
               except AttributeError:
                    preview = ''
               newItem = models.Item(title=subm.title,
                                        thumbnail=preview,
                                        nativeId=subm.id,
                                        description=subm.selftext,
                                        feed=feed)
               try:
                    newItem.save()
               except IntegrityError:
                    continue
     return feed
