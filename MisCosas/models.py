from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

LETTERCHOICES= [('x-small', 'Small'), ('medium', 'Medium'), ('x-large', 'Large')]
VOTECHOICES = [(1, 'Up'), (-1, 'Down')]
STYLECHOICES = [(True, 'Light'), (False, 'Dark')]
SHOWCHOICES = [(True, 'Yes'),(False, 'No')]


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    letterSize = models.CharField(choices=LETTERCHOICES,
                                    max_length=8, default = 'medium') 
    whiteStyle = models.BooleanField(default=True, choices=STYLECHOICES)
    picture = models.ImageField(upload_to="profiles/",
                                default = "profiles/default.jpg")
    feedRecord = models.ManyToManyField('Feed')

    def __str__(self):
        return self.user.get_username()

    @receiver(post_save, sender=User)  # hooks to link save method
    def createProfile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)


class FeedServer(models.Model):
    name = models.CharField(max_length = 64, primary_key=True)

    def __str__(self):
        return self.name


class Feed(models.Model):
    server = models.ForeignKey(to=FeedServer, on_delete = models.CASCADE)
    name = models.CharField(max_length = 64)  #channel name
    webId = models.CharField(max_length = 64)  #feed original id
    show = models.BooleanField(default=True, blank=True, choices=SHOWCHOICES)

    def length(self):
        return count(Feed.objects.get(channel=self.title).item_set.all())

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ['server', 'name']  # avoid duplicates
        ordering = ['name']


class Item(models.Model):
    title = models.CharField(max_length = 64)
    nativeId = models.CharField(max_length = 32)
    thumbnail = models.URLField()
    description = models.TextField(blank=True)
    feed = models.ForeignKey(to=Feed, on_delete = models.CASCADE)

    def puntuation(self):
        votes = self.vote_set.all()
        total = 0
        for vote in votes:
            total += vote.value
        return total

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ['title', 'feed']  # avoid duplicates
        ordering = ['-id']


class Comment(models.Model):
    profile = models.ForeignKey(to=Profile, on_delete = models.CASCADE)  # if profile is deleted, all comments as well
    item = models.ForeignKey(to=Item, on_delete = models.CASCADE)
    text = models.CharField(max_length=256)
    picture = models.ImageField(upload_to='comments/', blank=True)
    date = models.DateTimeField(auto_now_add = True)  # only creation

    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.item.title


class Vote(models.Model):
    value = models.IntegerField(choices=VOTECHOICES, default=0)
    profile = models.ForeignKey(to=Profile, on_delete=models.CASCADE)
    item = models.ForeignKey(to=Item, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)  # every time is modified

    class Meta:
        unique_together = ['profile', 'item','value'] # same user can't vote twice the same
        ordering = ['date']

    def __str__(self):
        return self.item.title
