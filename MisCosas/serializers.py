from rest_framework.serializers import ModelSerializer, Serializer, IntegerField
from .models import *


class ProfileSerializer(ModelSerializer):

    class Meta:
        model = Profile
        fields = ['user', 'picture']

class FeedSerializer(ModelSerializer):

    class Meta:
        model = Feed
        fields = ['name', 'show','server']


class ItemSerializer(ModelSerializer):

    class Meta:
        model = Item
        fields = ['title', 'nativeId', 'thumbnail', 'description', 'feed']


class ItemSerializer2(Serializer):
    item = ItemSerializer()
    downvotes = IntegerField()
    upvotes = IntegerField()
    total = IntegerField()


class VoteSerializer(ModelSerializer):

    class Meta:
        model = Vote
        fields = ['item', 'id']


class HomeSerializer(Serializer):
    profile = ProfileSerializer()
    top10 = ItemSerializer2(many=True)
    usertop5 = ItemSerializer2(many=True)
    userlast5 = VoteSerializer(many=True)
    feedlist = FeedSerializer(many=True)


class AnonymousHomeSerializer(Serializer):
    top10 = ItemSerializer2(many=True)
    feedlist = FeedSerializer(many=True)
