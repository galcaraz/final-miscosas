#by galcaraz
#Retrieve data & pages in general

from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseNotFound
from django.core import serializers
from .youtubeAPI import YouTubeSearchCN, YouTubeSearchID
from .redditAPI import RedditSearch
from .serializers import *
from .models import *
from .forms import *
from .users import getProfile


def getTop5(sortedList, profile):
    profileVotes = profile.vote_set.all()
    answer = []
    for item in sortedList:
        for vote in profileVotes:
            if vote.item.id == item['item'].id and vote.value != 0:
                answer.append(item)
                break
        if len(answer) == 5:
            break
    return answer


def home(request):
    outFormat = request.GET.get('format')
    profile = getProfile(request)
    itemList = Item.objects.all()
    aux = []
    if request.method =='POST':
        if "choose" in request.POST:  # user adds item to its record
            feedid = request.POST.get('choose')
            profile.feedRecord.add(feedid)
        elif "vote" in request.POST:  # user votes
            if profile :
                itemid = request.POST.get('vote')
                vote = Vote.objects.get(item=itemid, profile=profile)
                voteForm = VoteForm(request.POST, instance=vote)
                if voteForm.is_valid():
                    voteForm.save()
        else:  # anyone can hide items
            feedid = request.POST.get('feedid')
            Feed.objects.filter(id=feedid).update(show=False)
            if profile:  # if logged, he will remove it from his feedRecord
                profile.feedRecord.remove(feedid)
    for item in itemList:
        if profile:
            vote, created = Vote.objects.get_or_create(item=item, 
                                                        profile=profile)
            voteForm = VoteForm(instance=vote)
        else:
            voteForm=''
        votes = item.vote_set.all()
        aux.append({'item': item,
                    'upvotes': votes.filter(value=1).count(),
                    'downvotes': votes.filter(value=-1).count(),
                    'total' : item.puntuation(),
                    'voteform': voteForm})
    aux = sorted(aux, key=lambda i: i['total'], reverse=True)
    if profile:
        data = {"profile": profile, 
                "top10": aux[:10],
                "usertop5": getTop5(aux,profile),  # the top items voted by the user & community
                "userlast5": profile.vote_set.exclude(value=0)[:5],  # most recent will be last on DB
                "form": FeedForm(),
                'feedlist': list(Feed.objects.all())}  # listed to work on serializer
    else:
        data = {"top10": aux[:10],
                "form": FeedForm(),
                'feedlist': list(Feed.objects.all())} 
    if outFormat == "xml":
        return render(request, 'homexml.xml',
                    data, content_type='application/xml')
    elif outFormat == "json":
        if profile:
            ser = HomeSerializer(data)
        else:
            ser = AnonymousHomeSerializer(data)
        return JsonResponse(ser.data)
    else:
        return render(request, 'home.html', data)


def getFeed(request, id): # feed when received from anywhere except home form
    feed = Feed.objects.get(id=id)
    profile = getProfile(request)
    if request.method == 'POST':
        form = ShowFeed(request.POST, instance=feed)
        if form.is_valid():
            feed = form.save()
            if feed.show and profile:  # shown items will reincorporate to user's feedRecord
                profile.feedRecord.add(feed)  
            elif profile:
                profile.feedRecord.remove(feed)
        else:
            if request.POST.get("refresh"):
                server = str(feed.server)
                if server == 'YouTube':
                    feed = YouTubeSearchID(feed.webId, True)
                else:
                    feed = RedditSearch(feed.name, True)
    items = feed.item_set.all()
    totalPuntuation = 0
    for i in items:
        totalPuntuation += i.puntuation()
    data = {'profile': profile,
            'feed': feed,
            'showform': ShowFeed(instance=feed),
            'items': items,
            'numItems': items.count(),
            'totalPuntuation': totalPuntuation}
    return render(request, 'feedpage.html', data)


def getFeedForm(request):  # feed when received from home form
    if request.method == "POST":
        server = request.POST.get('server')
        name = request.POST.get('name')
        try:
            if server == "YouTube User":
                feed = YouTubeSearchCN(name)
            elif server == "YouTube":
                feed = YouTubeSearchID(name, False)
            elif server == "Reddit":
                feed = RedditSearch(name, False)
        except:
            return HttpResponseNotFound("<h1>Search error </h1>" +
                    "<a href='/'>Home</a>")
    items = feed.item_set.all()
    totalPuntuation = 0
    for i in items:
        totalPuntuation += i.puntuation()
    profile = getProfile(request)
    if profile:
        profile.feedRecord.add(feed)
    data = {'profile': profile,
            'feed': feed,
            'showform': ShowFeed(instance=feed),
            'items': items, 
            'numItems': items.count(),
            'totalPuntuation': totalPuntuation}
    return render(request, 'feedpage.html', data)


def getFeedList(request):
    data = {'profile': getProfile(request),
            'feedlist': Feed.objects.all()}
    return render(request, 'feedlist.html', data)


def getItem(request, key):
    item = get_object_or_404(Item, id=key)
    profile = getProfile(request)
    if profile:
        vote, created = Vote.objects.get_or_create(item=item, 
                                                    profile=profile)
        voteForm = VoteForm(instance=vote)
        commentForm = CommentForm(initial = {'item': item.id,
                                            'profile': profile.id})
    else:
        voteForm = commentForm = 0
    if request.method == 'POST':
        if "submitvote" in request.POST:
            voteForm = VoteForm(request.POST, instance=vote)
            if voteForm.is_valid():
                voteForm.save()
        else:
            form = CommentForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
    data = {'profile': profile,
            'item': item, 
            'comments': item.comment_set.all(),
            'voteform': voteForm,
            'puntuation': item.puntuation(),
            'commentform': commentForm}
    itemhtml = '{}item.html'.format(str(item.feed.server).lower())
    return render(request, itemhtml, data)


def infoPage(request):
    data = {'profile': getProfile(request)}
    return render(request, 'info.html', data)
