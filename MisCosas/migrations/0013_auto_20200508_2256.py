# Generated by Django 3.0.3 on 2020-05-08 20:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MisCosas', '0012_auto_20200507_2233'),
    ]

    operations = [
        migrations.RenameField(
            model_name='vote',
            old_name='user',
            new_name='profile',
        ),
        migrations.AlterField(
            model_name='feed',
            name='show',
            field=models.BooleanField(blank=True, choices=[(True, 'Yes'), (False, 'No')], default=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='whiteStyle',
            field=models.BooleanField(choices=[(True, 'Light'), (False, 'Dark')], default=True),
        ),
        migrations.AlterField(
            model_name='vote',
            name='value',
            field=models.IntegerField(choices=[('1', 'Up'), ('-1', 'Down')], default=0),
        ),
        migrations.AlterUniqueTogether(
            name='vote',
            unique_together={('profile', 'item', 'value')},
        ),
    ]
