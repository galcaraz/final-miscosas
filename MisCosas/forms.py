from django import forms
from .models import *


class FeedForm(forms.ModelForm):

    class Meta:
        model = Feed
        fields = ['server', 'name']


class ShowFeed(forms.ModelForm):
    show = forms.ChoiceField(label='Show feed?',
                            choices=SHOWCHOICES,
                            widget=forms.RadioSelect)

    class Meta:
        model = Feed
        fields = ['show']


class VoteForm(forms.ModelForm):
    value = forms.ChoiceField(label='Vote!',
                            choices=VOTECHOICES, 
                            widget=forms.RadioSelect)

    class Meta:
        model = Vote
        fields = ['value']


class ProfileForm(forms.ModelForm):
    picture = forms.ImageField(required=False, widget=forms.FileInput())
    whiteStyle = forms.ChoiceField(label="Style", choices=STYLECHOICES)

    class Meta:
        model = Profile
        exclude = ['feedRecord', 'user']


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = '__all__'
        widgets = {'profile': forms.HiddenInput(),
                    'item': forms.HiddenInput()}
