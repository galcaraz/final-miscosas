from django.test import TestCase, SimpleTestCase
from . import views, users
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth import login
from requests.models import Response

'''
Recurso             Method        Description
/                   GET           get home
                    POST          vote || hide
/item/(key)         GET           get item  
                    POST          vote || comment
/users              GET           get allusers
/users/logout       GET           logout
/users/log          GET           login
                    POST          signup
/users/(username)   GET           get Profile
                    POST          edit profile
/feedlist           GET           get all feeds
/feed               POST          get feed from form
/feed/(id)          GET           get feed from id
                    POST          show/hide feed
/info               GET           get info
'''

class TestAll(TestCase):
    def setUp(self):
        self.u1 = User.objects.create(username="testuser", password=1234)
        self.u2 = User.objects.create(username="testuser2", password=1234)
        self.p1 = Profile.objects.get(user=self.u1) #if works, signal hook works
        self.p2 = Profile.objects.get(user=self.u2)
        self.fs1 = FeedServer.objects.create(name="YouTube")
        self.fs2 = FeedServer.objects.create(name="Reddit")
        self.ch1 = Feed.objects.create(server=self.fs1, name="PewDiePie")
        self.ch2 = Feed.objects.create(server=self.fs1, name="Wismichu")
        self.ch3 = Feed.objects.create(server=self.fs1, name="loulogio")
        self.ch4 = Feed.objects.create(server=self.fs2, name="memes")
        self.ch5 = Feed.objects.create(server=self.fs2, name="help")
        self.it1 = Item.objects.create(title="VideoTest1", feed=self.ch1)
        self.it2 = Item.objects.create(title="VideoTest2", feed=self.ch2)
        self.it3 = Item.objects.create(title="VideoTest3", feed=self.ch3)
        self.it4 = Item.objects.create(title="RedditTest4", feed=self.ch4)
        self.v1 = Vote.objects.create(value=1, profile=self.p1, item=self.it1)
        self.v2 = Vote.objects.create(value=1, profile=self.p2, item=self.it1)
        self.v3 = Vote.objects.create(value=1, profile=self.p1, item=self.it2)
        self.v4 = Vote.objects.create(value=-1, profile=self.p2, item=self.it2)
        self.itemUrl='/item/' + str(self.it2.id) + '/'
        self.feedUrl='/feed/' + str(self.ch3.id) + '/'
        self.profileUrl='/users/' + str(self.p2) + '/'


#---------------------HOME--------------------------------------#
    def testGEThome(self): #checks GET & anonnymous user
        response = self.client.get('/')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, '<label for="login">Login</label>')

    def testPOSThomelogged(self):  #checks vote & logged user
        response = self.client.post('/', {'vote':1, 'value':1},USER=self.u1,
                        content_type="application/x-www-form-urlencoded", follow=True)
        self.assertEquals(response.status_code, 200)
        
    def testPOSThomeAnonnymous(self): #checks hide 
        response = self.client.post('/', {'feedid':self.ch2.id},
                        content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)

#---------------------PROFILES/USERS--------------------------------------#
    def testGETuserList(self): #checks GET & name found
        response = self.client.get('/users/')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.p2)

    def testGETprofile(self): #checks GET & name found
        response = self.client.get(self.profileUrl)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.p2)

    def testGETprofile(self): #checks GET & name found
        response = self.client.get(self.profileUrl)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.p2)

    def testPOSTprofile(self): #feed search
        form=FeedForm(data={"server":self.fs1, "name":self.ch1})
        response = self.client.post(self.feedUrl, form, content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)

#---------------------ITEM--------------------------------------#

    def testGETitem(self): #checks GET & anonnymous user
        response = self.client.get(self.itemUrl)
        self.assertEquals(response.status_code, 200)

    def testPOSTvoteItem(self):  #checks vote logged
        response = self.client.post(self.itemUrl, {'value':-1, 
                        'submitvote':'submitvote'}, USER=self.u1,
                        content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)

    def testPOSTcommentItem(self): #checks comment logged 
        response = self.client.post(self.itemUrl, {'text':"HelloWorld", 
                        'profile':self.p1.id, 
                        'item':self.it2.id}, USER=self.u1,
                        content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)


#---------------------FEED--------------------------------------#
    def testGETfeed(self): #checks GET & anonnymous user
        response = self.client.get(self.feedUrl)
        self.assertEquals(response.status_code, 200)

    def testPOSTfeed(self): #feed search
        data={"server":str(self.fs1), "name":str(self.ch1)}
        response = self.client.post("/feed", data)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.ch1.name)

    def testPOSTfeedHide(self): #checks hide feed 
        form=ShowFeed(data={"show":False})
        response = self.client.post(self.feedUrl, form, content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)

    def testPOSTfeedHide(self): #checks hide feed 
        form=ShowFeed(data={"show":True})
        response = self.client.post(self.feedUrl, form, content_type="application/x-www-form-urlencoded")
        self.assertEquals(response.status_code, 200)

#---------------------FEEDLIST--------------------------------------#
def testGETfeedList(self): #checks GET & anonnymous user
        response = self.client.get('/feedlist')
        self.assertEquals(response.status_code, 200)

#---------------------INFO--------------------------------------#
def testGETinfo(self): #checks GET & anonnymous user
        response = self.client.get('/info')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "info.html")
