#by galcaraz
#gets Channel and videos from YouTubeAPI

import requests
from django.conf import settings #import private token
from django.db.utils import IntegrityError
from . import models


URLSEARCH = 'https://www.googleapis.com/youtube/v3/search'
URLCHANNELS = 'https://www.googleapis.com/youtube/v3/channels'


def YouTubeSearchID(id, refresh):
    channelName = getChannel(id)
    return YouTubeSearch(channelName, id, refresh)


def YouTubeSearchCN(username):
    print(username)
    channelId = getId(username)
    print("ojo:" + channelId)
    channelName = getChannel(channelId)
    print(channelName)
    return YouTubeSearch(channelName, channelId, False)


def YouTubeSearch(channelName, channelId, refresh): 
    feed, created = models.Feed.objects.get_or_create(
                                    server=models.FeedServer("YouTube"),
                                    name=channelName,
                                    webId=channelId)
    if created or refresh:
        searchVideos(feed, channelId)
    return feed


def getId(username):  # returns channelId from channel
    params = {
        'key': settings.YOUTUBE_DATA_API_KEY,
        'part': 'contentDetails',
        'forUsername': username,
        'maxResults': 1  #best result only
    }
    r = requests.get(URLCHANNELS, params=params)
    return r.json()['items'][0]['id']


def getChannel(channelId):
    params = {
        'key': settings.YOUTUBE_DATA_API_KEY,
        'part': 'snippet',
        'id': channelId,
        'maxResults': 1  #best match only
    }
    r = requests.get(URLCHANNELS, params=params)
    answer = r.json()['items']
    return answer[0]['snippet']['title']


def searchVideos(feed, channelId):
    params = {
        'key' : settings.YOUTUBE_DATA_API_KEY,
        'part' : 'snippet',
        'channelId' : channelId,
        'order': 'date',
        'maxResults' : settings.MAX_RESULTS,  # for optimization
        'type': 'video'
    }
    r =requests.get(URLSEARCH, params = params)
    videoList = r.json()['items']
    for i in videoList:
        newItem = models.Item(title=i['snippet']['title'],
                    nativeId=i['id']['videoId'],
                    thumbnail=i['snippet']['thumbnails']['default']['url'],
                    description=i['snippet']['description'],
                    feed=feed)
        try:
            newItem.save()
        except IntegrityError:
            continue
    return
