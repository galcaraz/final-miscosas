# Entrega practica

## Datos

* Nombre:Guillermo Alcaraz López
* Titulación:IST+ADE
* Despliegue (url): http://galcarazl.pythonanywhere.com/
* Video básico (url):https://youtu.be/jSOKlQF1Keg?list=PLodXGLpvlCf4wyQox3BJFiNzyLvavSjIe
* Video parte opcional (url):https://youtu.be/PXCsISIQ3_Y?list=PLodXGLpvlCf4wyQox3BJFiNzyLvavSjIe

* Importante: Por razones de seguridad, a dia 1/07/2020, la llave de GoogleApi y de Reddit la cambiaré, dado que no es conveniente que esté publicada de esta forma. Después podrá seguir funcionando si el programador oportuno cambia este campo en settings.py
## Cuenta Admin Site

* admin/admin

## Cuentas usuarios (username/password)

* hola/hola
* adios/adios
* yaoming/yaoming
* federico/hola
* pepe/pepe
* papi/papi
* yo/yo

## Resumen parte obligatoria.

Se ha construido un portal llamado MisCosas, donde se pueden seleccionar, votar o comentar items.
En la página principal se encuentran los listados de los items más votados, los alimentadores seleccionados y los 5 elementos votados más recientes del usuario.
También podremos buscar items antiguos o nuevos desde el formulario que hay en la página. La página soporta el ID del canal de Youtube (opción "YouTube") o el subrreddit
(opción "Reddit"), además de otra opción que se describe en la parte opcional.

En la página del alimentador se muestran sus items, con la posibilidad de refrescarlos y añadir los más recientes. 
También se permite la opción de ocultar el alimentador de la página principal. Siempre está la posibilidad de ver el alimentador original, seleccionando su título.
Cuando se accede a un item, se pude votar, comentar y por supuesto, ver.

En el menú superior hay una serie de enlaces: lista de alimentadores (feedlist), vuelta a la página principal (home), perfil del usuario si está logeado (profile),
la lista de usuarios de la comunicad (community), y una breve información (info). Cuando se está en esa misma página no se muestra su mismo link.

Si se crea un nuevo usuario o se identifica uno antiguo, se mantiene en la misma página. 
Además cada usuario podrá personalizar la página a su gusto eligiendo el modelo "light" o "dark", el tamaño de la letra o su foto de perfil,
que podrán ver otros usuarios cuando comente o le busquen en la comunidad.
Para acceder a las funciones de comentar, votar, o cambiar los valores de tu perfil necesitan que el usuario esté registrado

La página principal se puede descargar en XML o JSON, poniendo detrás de la direción las queries: "?format=xml" o "?format=json" según corresponda.

Un programador desde su terminal podrá correr una serie testeos de la aplicación, localizados en "test.py"


La aplicación la he querido construir con una interfaz en inglés por si deseo compartirla en mis redes profesionales.

## Lista partes opcionales

* favicon: Favicon introducido.
* formulario para introducir imágenes en los comentarios: a través de ImageField.
* FeedRecord: Historial de alimentadores seleccionados, un ManyToManyField, que es dinámico según el usuario oculta alimentadores de la lista. principal. Si otro usuario modifica la lista principal, no a fecta a la lista de alimentadores seleccionados personal.
* Youtube User: Búsqueda de usuarios (cada canal pertenece a un usuario, y en ocasiones son homónimos).
* TOP5 de los items votados por el usuario: En la página principal,se muestra un top de los items votados por el usuario que tienen puntuaciones. totales más altas, y se puede cambiar el voto, aparte del otro top5 de la parte obligatoria.
* Máximo de búsquedas editable: En settings en la variable MAX_RESULTS.
* Uso de Google API y Reddit API: En vez de los parseadores de clase.
* Uso de señales para el guardado de perfiles: cuando se recibe una señal postsave de un user se crea un perfil.
* Procesadores en serie o "serializers": Para la extracción de datos en JSON de la página principal .
* Protección ante algunos errores: como la creación de un usuario ya existente, o la búsqueda de un canal que no existe en youtube o Reddit.
* Mejor movilidad: La imagen del banner redirige al home, las fotos de alimentadores o items redirigen a su elemento.
* En la página de un alimentador hay una preview o thumbnail de cada item: si no la tuviera, se le asigna una por default.
* Página de información ampliada: he añadido los videos explicativos de la parte obligatoria y opcional.
* Modo debug a falso: Los errores saldrán con mensajes de python anywhere, en vez de la página de debug de Django.
